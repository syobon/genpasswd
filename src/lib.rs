use rand::prelude::*;
use rand_chacha::ChaCha20Rng;
use std::collections::HashSet;
use wasm_bindgen::prelude::*;

const ALPHABETS: [char; 26] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
];
const ASCII_SPECIALS: [char; 32] = [
    '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=',
    '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{', '|', '}', '~',
];
const BITWARDEN_COMPATIBLES: [char; 8] = ['!', '@', '#', '$', '%', '^', '&', '*'];

#[wasm_bindgen]
pub fn gen_passwd(types: u32, limitation: u32, extra: String, length: u32) -> String {
    if types == 0 && extra.is_empty() {
        return "* Invalid Parameters.".to_string();
    }

    let chars = gen_char_list(types, extra.clone());
    let mut passwd = gen_random_passwd(chars.clone(), length);

    if limitation >= 1 {
        // Do not allow consecutive same characters
        if types == 0 && extra.clone().chars().collect::<HashSet<char>>().len() < 2 {
            return "* Invalid Parameters.".to_string();
        }
        for i in 1..length {
            while passwd.chars().nth(i as usize) == passwd.chars().nth((i - 1) as usize) {
                passwd.replace_range(
                    passwd
                        .char_indices()
                        .nth(i as usize)
                        .map(|(pos, ch)| (pos..pos + ch.len_utf8()))
                        .unwrap(),
                    &gen_random_passwd(chars.clone(), 1),
                );
            }
        }
    }

    passwd
}

pub fn gen_random_passwd(chars: Vec<char>, length: u32) -> String {
    let mut passwd: String = "".to_string();
    let rand_vec = gen_rand(chars.len().try_into().unwrap(), length);
    for i in rand_vec {
        passwd.push(chars[i as usize]);
    }

    passwd
}

fn gen_rand(length: u32, count: u32) -> Vec<u32> {
    let mut rand_vec: Vec<u32> = vec![];

    let mut rng = ChaCha20Rng::from_entropy();
    for _ in 0..count {
        rand_vec.push(rng.gen_range(0..length));
    }

    rand_vec
}

fn gen_char_list(mut types: u32, extra: String) -> Vec<char> {
    /* types
     *    1 - small alphabets
     *    2 - large alphabets
     *    4 - numbers
     *    8 - Bitwarden compatible
     *   16 - ascii special characters
     *   32 - half-width katakana
     *   64 - hiragana
     *  128 - katakana
     *  256 - CJK Unified Ideographs
     *  512 - CJK Unified Ideographs Extension A
     * 1024 - Hangul Syllables
     */
    let mut chars: Vec<char> = vec![];
    if types >= 2048 {
        types = 2047;
    }
    if types >= 1024 {
        for i in 44032..=55203 {
            // Hangul Syllables is from U+AC00 to U+D7A3
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 1024;
    }
    if types >= 512 {
        for i in 13312..=19893 {
            //CJK Unified Ideographs Extension A is from U+3400 to U+4DB5
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 512
    }
    if types >= 256 {
        for i in 19968..=40938 {
            // CJK Unified Ideographs is from U+4E00 to U+9FEA
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 256;
    }
    if types >= 128 {
        for i in 12449..=12531 {
            // Katakana is from U+30A0 to U+30FF. However, we don't use U+30A0
            // and U+30FA~U+30FF because they're so rare
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 128;
    }
    if types >= 64 {
        for i in 12353..=12435 {
            // Hiragana is from U+3041 to U+309F. However, we don't use
            // U+3094~U+309F because they're so rare
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 64;
    }
    if types >= 32 {
        for i in 65382..=65437 {
            // Half-width Katakana is from U+FF66 to U+FF9D in Halfwidth and
            // Fullwidth Forms
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 32;
    }
    if types == 31 {
        for i in 33..=126 {
            // Visible characters in Basic Latin is from U+0021 to U+007E
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 31;
    }
    if types >= 16 {
        for c in ASCII_SPECIALS {
            chars.push(c);
        }
        types -= 16;
        if types >= 8 {
            // Ascii special characters contain Bitwarden compatible characters
            types -= 8;
        }
    }
    if types >= 8 {
        for c in BITWARDEN_COMPATIBLES {
            chars.push(c);
        }
        types -= 8;
    }
    if types >= 4 {
        for i in 48..=57 {
            chars.push(char::from_u32(i).unwrap());
        }
        types -= 4;
    }
    if types >= 2 {
        for c in ALPHABETS.map(|x| x.to_ascii_uppercase()) {
            chars.push(c);
        }
        types -= 2;
    }
    if types >= 1 {
        for c in ALPHABETS {
            chars.push(c);
        }
    }
    for c in extra.chars() {
        chars.push(c);
    }

    chars
}
